package controlador;
//le agregue

//lo que tenia
import java.sql.*;
/*le agregue*/
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.LinkedList;

@Controller
public class LoginControlador {

    //agruegue
    private Connection connection = null;
    private ResultSet rs = null;
    private Statement s = null;
    public Integer cedula_anterior;
    public Integer codProyecto_anterior;
    private Integer codUserStory_anterior;
    private Integer CodUS_anterior;
    private String estado = "cancelado";
    private Integer codSprintA;
    private Integer rol; 

    //tenia
    public void Conexion() {
        if (connection != null) {
            return;
        }

        String url = "jdbc:postgresql://localhost:5432/IS2";
        String usuario = "posgrest";
        String password = "Miguel_18";

        try {
            Class.forName("org.postgresql.Driver");

            connection = DriverManager.getConnection(url, usuario, password);

            if (connection != null) {
                System.out.println("Conectado a Base de Datos...");
            }

        } catch (Exception e) {
            System.out.println("Error al registrar el driver de PostgreSQL: ");
        }

    }

    // matias parte
    @RequestMapping(value = "/login")
    public String recibir(@RequestParam("usuario") String us, @RequestParam("password") String pass, Model m) {
        String usuario = " ";
        String password = " ";
        String mensaje = " ";
        String vista = " ";
        rs = null;
        Conexion();
        try {
            s = connection.createStatement();
            String SSQL = "select * from personas where nombre_usuario = '" + us + "'and contrasenha ='" + pass + "';";
            rs = s.executeQuery(SSQL);
            while (rs.next()) {
                usuario = rs.getString(4);
                password = rs.getString(5);
                rol = rs.getInt(2);
            }

            if (us.equals(usuario) && pass.equals(password)) {
                mensaje = "Bienvenido Login Correcto!";
                m.addAttribute("bienvenido", mensaje);
                vista = "inicio";
            } else {
                mensaje = "Usuario o Contraseña incorrecto!";
                m.addAttribute("validacion", mensaje + usuario + password);
                vista = "index";
            }

        } catch (Exception e) {
            System.out.println("Problema buscando en la base de datos");
        }

        if (us.equals("")) {
            mensaje = "Error los campos no se deben dejar en blanco!";
            m.addAttribute("err", mensaje);
            vista = "error";
        }
        return vista;
    }

    //miguel parte
    @RequestMapping(value = "/inicioUsuario")
    public String inicioUsuario() {
        return "inicioUsuario";
    }

    @RequestMapping(value = "/inicioProyecto")
    public String inicioProyecto() {
        return "inicioProyecto";
    }

    @RequestMapping(value = "/inicioProductBacklog")
    public String inicioProductBacklog() {
        return "inicioProductBacklog";
    }

    @RequestMapping(value = "/inicioSprintBacklog")
    public String inicioSprintBacklog() {
        return "inicioSprintBacklog";
    }
    @RequestMapping(value = "/inicioSprintBacklog2")
    public String inicioSprintBacklog2() {
        return "inicioSprintBacklog2";
    }
    @RequestMapping(value = "/form")
    public String showUserForm(Model model) {
        model.addAttribute("usuario", new Usuario());
        return "alta_usuario";
    }
    

    @RequestMapping(value = "/insert.htm")
    public String insertarUsuario(Usuario user) {
        Conexion();
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("insert into personas values ('" + user.cedula + "','" + user.id_rol +"','" + user.nombres + "','" + user.apellidos + "','" + user.nombre_usuario + "','" + user.contrasenha + "','" + user.telefono + "','activo');");
            if (z == 1) {
                System.out.println("Se agrego el registro de manera exitosa");
            } else {
                System.out.println("Ocurrio un error en la insercion");
            }
        } catch (SQLException e) {
           return "mensaje_error";
        }
        return "mensaje";
    }

    @RequestMapping(value = "/form2")
    public String showUserForm2(Model model) {
        model.addAttribute("usuario", new Usuario());
        return "baja_usuario";
    }

    

    @RequestMapping(value = "/delete.htm")
    public String deleteUsuario(Usuario user) {
        Conexion();
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("update personas set estado = '" + "inactivo" + "'where cedula ='" + user.cedula + "';");
            if (z > 0) {
                System.out.println("Se elimino el registro de manera exitosa");
            } else {
                System.out.println("Ocurrio un error en la eliminacion");
            }

        } catch (SQLException e ) {
           return "mensaje_error";
        }
        return "mensaje";
    }

    @RequestMapping(value = "/form3")
    public String showUserForm3(Model model) {
        model.addAttribute("usuario", new Usuario());
        return "seleccion_usuario";
    }
     @RequestMapping(value = "/create3")
    public String manejarUsuarioValidado3(Usuario user) {
        if (user.cedula == null) {
            return "mensaje_error";//poder cambiar por una vista que imprima mensaje de campo vacio
        } else {
            return selectUsuario(user).getViewName();
            
        }
    }
    

    @RequestMapping(value = "/select.htm")
    public ModelAndView selectUsuario(Usuario user)  {
        Conexion();
        try {
            s = connection.createStatement();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from personas where cedula = '" + user.cedula +"' and estado != 'inactivo';");
            cedula_anterior = user.cedula;
            while (rs.next()) {
                user.id_rol = rs.getInt(2);
                user.nombres = rs.getString(3);
                user.apellidos = rs.getString(4);
                user.nombre_usuario = rs.getString(5);
                user.contrasenha = rs.getString(6);
                user.telefono = rs.getString(7);
                user.estado = rs.getString(8);
            }
        } catch (SQLException e) {
            System.err.println("Error de conexion");
        }
        ModelAndView model = new ModelAndView();
        model.addObject("usuario", user);
        model.setViewName("modificacion_usuario");

        return model;
    }

    
    @RequestMapping(value = "/update.htm")
    public String updateUsuario(Usuario user) {
        Conexion();
        try {
            int z = s.executeUpdate("update personas set cedula = '" + user.cedula
                    + "',id_rol = '" + user.id_rol
                    + "',nombres = '" + user.nombres
                    + "',apellidos = '" + user.apellidos
                    + "',nombre_usuario = '" + user.nombre_usuario
                    + "',contrasenha = '" + user.contrasenha
                    + "',telefono = '" + user.telefono
                    + "' where cedula = '" + cedula_anterior
                    + "' and estado != 'inactivo' "
                    + ";");
            if (z > 0) {
                System.out.println("Se actualizo el registro de manera exitosa");
            } else {
                System.out.println("Ocurrio un error en la actualizacion");
            }
        } catch (Exception e) {
            return "mensaje_error";
        }
        return "mensaje";
    }

//--------------------------------------------------------PROYECTO--------------------------------------------------------    
    @RequestMapping(value = "/formProyecto1")
    public String showUserFormProyecto(Model model) {
        model.addAttribute("proyecto", new Proyecto());
        return "alta_proyecto";
    }

    @RequestMapping(value = "/insertProyecto.htm")
    public String insertarProyecto(Proyecto p) {
        Conexion();
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("insert into proyecto values ('" + p.codigo_proyecto
                                                                + "','" + p.nombre_proyecto
                                                                + "','creado'"
                                                                + ",to_date('" + p.fecha_inicio + "','YYYYMMDD')"
                                                                + ",to_date('" + p.fecha_fin + "','YYYYMMDD'));");
            if (z == 1) {
                System.out.println("Se agrego el registro de manera exitosa");
            } else {
                System.out.println("Ocurrio un error en la insercion");
            }
        } catch (SQLException e) {
            return "mensaje_error";
        }
        return "mensaje";
    }

    @RequestMapping(value = "/formProyecto2")
    public String showUserFormProyecto2(Model model) {
        model.addAttribute("proyecto", new Proyecto());
        return "baja_proyecto";
    }

    @RequestMapping(value = "/deleteProyecto.htm")
    public String deleteProyecto(Proyecto p) {
        Conexion();
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("update proyecto set estado_proyecto = 'cancelado' where codigo_proyecto ='" + p.codigo_proyecto + "';");
            if (z > 0) {
                System.out.println("Se elimino el registro de manera exitosa");
            } else {
                System.out.println("Ocurrio un error en la eliminacion");
            }

        } catch (SQLException e) {
            return "mensaje_error";
        }
        return "mensaje";
    }

    @RequestMapping(value = "/formProyecto3")
    public String showUserFormProyecto3(Model model) {
        model.addAttribute("proyecto", new Proyecto());
        return "seleccion_proyecto";
    }

    @RequestMapping(value = "/create7")
    public String manejarProyectoValidado3(Proyecto p) {
        if (p.codigo_proyecto == null) {
            return "mensaje_error";//poder cambiar por una vista que imprima mensaje de campo vacio
        } else {
            return selectProyecto(p).getViewName();
            
        }
    }

    @RequestMapping(value = "/selectP.htm")
    public ModelAndView selectProyecto(Proyecto p) {
        Conexion();
        try {
            s = connection.createStatement();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from proyecto where codigo_proyecto = '" + p.codigo_proyecto +"' and estado_proyecto != 'cancelado';");
            codProyecto_anterior = p.codigo_proyecto;
            while (rs.next()) {
                p.nombre_proyecto = rs.getString(2);
                p.estado = rs.getString(3);
                p.fecha_inicio = rs.getString(4);
                p.fecha_fin = rs.getString(5);
            }
        } catch (Exception e) {
            System.out.println("Error de conexion");
        }
        ModelAndView model = new ModelAndView();
        model.addObject("proyecto", p);
        model.setViewName("modificacion_proyecto");

        return model;
    }
  
    @RequestMapping(value = "/updateProyecto.htm")
    public String updateProyecto(Proyecto p) {
        Conexion();
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("update proyecto set codigo_proyecto = '" + p.codigo_proyecto
                                                    + "', nombre_proyecto = '" + p.nombre_proyecto
                                                    + "', fecha_inicio = to_date('" + p.fecha_inicio + "','YYYYMMDD')"
                                                    + ", fecha_fin = to_date('" + p.fecha_fin + "','YYYYMMDD')"
                                                    + " where codigo_proyecto = '" + codProyecto_anterior
                                                    + "' and estado_proyecto !='" + estado + "';");
            if (z > 0) {
                System.out.println("Se actualizo el registro de manera exitosa");
            } else {
                System.out.println("Ocurrio un error en la actualizacion");
            }
        } catch (SQLException e ) {
            return "mensaje_error";
        }
        return "mensaje";
    }

//--------------------------------------------------------PRODUCT_BACKLOG--------------------------------------------------------        
    @RequestMapping(value = "/formProductBacklog1")
    public String showUserFormProductBacklog(Model model) {
        model.addAttribute("productbacklog", new Product_Backlog());
        return "alta_productbacklog";
    }
    

    

    @RequestMapping(value = "/insertProductBacklog.htm")
    public String insertarProductBacklog(Product_Backlog pb) {
        Conexion();
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("insert into product_backlog values ('" + pb.codigo_proyecto
                                                                    + "','" + pb.codigo_us
                                                                    + "','" + "creado"
                                                                    + "','" + pb.prioridad
                                                                    + "',to_timestamp('" + pb.tiempo_estimado + "','HH24:MI:SS')"
                                                                    + ",'" + pb.tipo_us
                                                                    + "','" + pb.nombre_us
                                                                    + "');");

            if (z == 1) {
                System.out.println("Se agrego el registro de manera exitosa");
            } else {
                System.out.println("Ocurrio un error en la insercion");
            }
        } catch (SQLException e) {
            return "mensaje_error";
        }
        return "mensaje";
    }

    @RequestMapping(value = "/formProductBacklog2")
    public String showUserFormProductBacklog2(Model model) {
        model.addAttribute("productbacklog", new Product_Backlog());
        return "baja_productbacklog";
    }

    @RequestMapping(value = "/deleteProductBacklog.htm")
    public String deleteProductBacklog(Product_Backlog pb) {
        Conexion();
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("update product_backlog set estado_us = 'cancelado' where codigo_proyecto ='" + pb.codigo_proyecto + "'and codigo_us = '" + pb.codigo_us + "';");
            if (z > 0) {
                System.out.println("Se elimino el registro de manera exitosa");
            } else {
                System.out.println("Ocurrio un error en la eliminacion");
            }

        } catch (SQLException e) {
            return"mensaje_error";
        }
        return "mensaje";
    }

    @RequestMapping(value = "/formProductBacklog3")
    public String showUserFormProductBacklog3(Model model) {
        model.addAttribute("productbacklog", new Product_Backlog());
        return "seleccion_productbacklog";
    }

    /* @RequestMapping(value="/create11")
    public String manejarPBValidado3(Product_Backlog pb) {
        if(pb.codigo_proyecto ==null || pb.codigo_us ==null){//no se sabe que onda
            return "mensaje";//poder cambiar por una vista que imprima mensaje de campo vacio
        }else{
            return selectProductBacklog(pb).getViewName();
            //return "error";
        } 
    }*/
    @RequestMapping(value = "/selectProductBacklog.htm")
    public ModelAndView selectProductBacklog(Product_Backlog pb) {
        Conexion();
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from product_backlog where codigo_proyecto = '" + pb.codigo_proyecto
                                                                                + "' and codigo_us = '" + pb.codigo_us 
                                                                                + "' and estado_us != 'cancelado';");
            codProyecto_anterior = pb.codigo_proyecto;
            CodUS_anterior = pb.codigo_us;
            while (rs.next()) {
                pb.estado_us = rs.getString(3);
                pb.prioridad = rs.getString(4);
                pb.tiempo_estimado = rs.getString(5);
                pb.tipo_us = rs.getString(6);
                pb.nombre_us = rs.getString(7);
            }
        } catch (SQLException e) {
            System.out.println("Error de conexion");
        }
        ModelAndView model = new ModelAndView();
        model.addObject("productbacklog", pb);
        model.setViewName("modificacion_productbacklog");

        return model;
    }

    @RequestMapping(value = "/updateProductBacklog.htm")
    public String updateProductBacklog(Product_Backlog pb) {
        Conexion();
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("update product_backlog set codigo_proyecto = '" + pb.codigo_proyecto
                                                                + "',codigo_us = '" + pb.codigo_us
                                                                + "',prioridad = '" + pb.prioridad
                                                                + "',tiempo_estimado = to_timestamp('" + pb.tiempo_estimado + "','HH24:MI:SS')"
                                                                + ",tipo_us = '" + pb.tipo_us
                                                                + "',nombre_us = '" + pb.nombre_us
                                                                + "' where codigo_proyecto = '" + codProyecto_anterior
                                                                + "' and codigo_us = '" + CodUS_anterior
                                                                + "' and estado_us != '" + estado
                                                                + "' ;");
            if (z > 0) {
                System.out.println("Se actualizo el registro de manera exitosa");
            } else {
                System.out.println("Ocurrio un error en la actualizacion");
            }
        } catch (SQLException e) {
            return "mensaje_error";
        }
        return "mensaje";
    }

    public LinkedList ConsultaP() {
        LinkedList<Proyecto> listaP = new LinkedList<Proyecto>();
        Conexion();
        try {
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("select * from Proyecto");
            while (rs.next()) {
                Proyecto p = new Proyecto();
                p.setCodigo_proyecto(rs.getInt("codigo_proyecto"));
                p.setNombre_proyecto(rs.getString("nombre_proyecto"));
                p.setEstado(rs.getString("estado_proyecto"));
                listaP.add(p);
            }
            rs.close();
            st.close();
        } catch (NumberFormatException | SQLException e) {
        }
        return listaP;
    }

    @RequestMapping(value = "/ConsultarPr")
    public String ConsultarProyecto() {
        return "ConsultaProyecto";
    }
    @RequestMapping(value = "/formProductBacklog4")
    public String showUserFormProductBacklog4(Model model) {
        model.addAttribute("productbacklog", new Product_Backlog());
        return "ConsultaUS";
    }
     public LinkedList ConsultaUS() {
        LinkedList<Product_Backlog> listaUS = new LinkedList<Product_Backlog>();
        Conexion();
        try {
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("select * from Product_Backlog");
            while (rs.next()) {
                Product_Backlog pb = new Product_Backlog();
                pb.setCodigo_proyecto(rs.getInt("codigo_proyecto"));
                pb.setCodigo_us(Integer.parseInt(rs.getString("codigo_us")));
                pb.setEstado_us(rs.getString("estado_us"));
                pb.setPrioridad(rs.getString("prioridad"));
                pb.setTiempo_estimado(rs.getString("tiempo_estimado"));
                pb.setTipo_us(rs.getString("tipo_us"));
                pb.setNombre_us(rs.getString("nombre_us"));
                listaUS.add(pb);
            }
            rs.close();
            st.close();
        } catch (NumberFormatException | SQLException e) {
        }
        return listaUS;
    }

    @RequestMapping(value = "/ConsultarUS")
    public String ConsultarUS() {
        return "ConsultaUS";
    }
//--------------------------------------------------------SPRINT_BACKLOG--------------------------------------------------------        

    @RequestMapping(value = "/formSprintBacklog1")
    public String showUserFormSpringBacklog(Model model) {
        model.addAttribute("sprintbacklog", new Sprint_Backlog());
        return "alta_sprintbacklog";
    }

    @RequestMapping(value = "/insertSprintBacklog.htm")
    public String insertarSprintBacklog(Sprint_Backlog sb) {
        Conexion();
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("insert into sprint_backlog values('" + sb.codigo_proyecto
                                                                    +"','"+ sb.codigo_us  
                                                                    + "','" + sb.cod_sp  
                                                                    + "','EL SPRINT NO DEBE TENER NOMBRE'"
                                                                    + " , 'por hacer' "
                                                                    + " , to_date('" + sb.fecha_inicio + "','YYYYMMDD')"
                                                                    + " , to_date('" + sb.fecha_limite + "','YYYYMMDD')"
                                                                    + " , to_timestamp('" + sb.tiempo_trabajado + "','HH24:MI:SS'));");
            if (z == 1) {
                System.out.println("Se agrego el registro de manera exitosa");
            } else {
                System.out.println("Ocurrio un error en la insercion");
            }
        } catch (SQLException e) {
           return "mensaje_error";
        }
        return "mensaje";
    }

    @RequestMapping(value = "/formSprintBacklog2")
    public String showUserFormSprintBacklog2(Model model) {
        model.addAttribute("sprintbacklog", new Sprint_Backlog());
        return "baja_sprintbacklog";
    }

    @RequestMapping(value = "/deleteSprintBacklog.htm")
    public String deleteSprintBacklog(Sprint_Backlog sb) {
        Conexion();
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("update sprint_backlog set estado_sp = 'cancelado' where codigo_proyecto = '" + sb.codigo_proyecto + "' and codigo_us = '" + sb.codigo_us + "';");
            if (z > 0) {
                System.out.println("Se elimino el registro de manera exitosa");
            } else {
                System.out.println("Ocurrio un error en la eliminacion");
            }

        } catch (SQLException e) {
            return "mensaje_error";
        }
        return "mensaje";
    }

    @RequestMapping(value = "/formSprintBacklog3")
    public String showUserFormSprintBacklog3(Model model) {
        model.addAttribute("sprintbacklog", new Sprint_Backlog());
        return "seleccion_sprintbacklog";
    }

    /* @RequestMapping(value="/create14")
    public String manejarSBValidado3(Sprint_Backlog sb) {
        if(sb.codigo_proyecto==null || sb.codigo_us ==null){//no se sabe que onda
            return "mensaje_error";//poder cambiar por una vista que imprima mensaje de campo vacio
        }else{
            return selectSprintBacklog(sb).getViewName();
            //return "mensaje";
        } 
    }
     */
    @RequestMapping(value = "/selectSprintBacklog.htm")
    public ModelAndView selectSprintBacklog(Sprint_Backlog sb) {
        Conexion();

        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from sprint_backlog where codigo_proyecto ='" + sb.codigo_proyecto
                                                                                +"'and codigo_us ='" + sb.codigo_us 
                                                                                +"and estado_sp != 'cancelado';");
            codProyecto_anterior = sb.codigo_proyecto;
            CodUS_anterior = sb.codigo_us;

            while (rs.next()) {
                sb.fecha_limite = rs.getString(3);
                sb.estado_sp = rs.getString(4);
                sb.fecha_inicio = rs.getString(5);
                sb.tiempo_trabajado = rs.getString(6);
            }

        } catch (NumberFormatException | SQLException e) {
            System.out.println("Error de conexion");
        }
        ModelAndView model = new ModelAndView();
        model.addObject("sprintbacklog", sb);
        model.setViewName("modificacion_sprintbacklog");
        return model;
    }

    @RequestMapping(value = "/updateSprintBacklog.htm")
    public String updateSprintBacklog(Sprint_Backlog sb) {
        Conexion();

        try {

            s = connection.createStatement();
            int z = s.executeUpdate("update sprint_backlog "
                    + "set codigo_proyecto = '" + sb.codigo_proyecto
                    + "',codigo_us = '" + sb.codigo_us
                    + "',estado_sp = '" + sb.estado_sp
                    + "',fecha_limite = to_date('" + sb.fecha_limite + "','YYYYMMDD')"
                    + "',fecha_inicio = to_date('" + sb.fecha_inicio + "','YYYYMMDD')"
                    + ",tiempo_trabajado = to_timestamp('" + sb.tiempo_trabajado + "','HH24:MI:SS') "
                    + "where codigo_proyecto = '" + codProyecto_anterior
                    + "' and codigo_us ='" + CodUS_anterior
                    + "' and estado_sp !='" + estado
                    + "' ;");
            if (z > 0) {
                System.out.println("Se actualizo el registro de manera exitosa");
            } else {
                System.out.println("Ocurrio un error en la actualizacion");
            }
        } catch (SQLException e) {
           return "mensaje_error";
        }
        return "mensaje";
    }
    @RequestMapping(value = "/formSprintBacklog4")
    public String showUserFormSprintBacklog4(Model model) {
        model.addAttribute("sprintbacklog", new Sprint_Backlog());
        return "ConsultaSprint";
    }
     public LinkedList ConsultaSprint() {
        LinkedList<Sprint_Backlog> listasp = new LinkedList<Sprint_Backlog>();
        Conexion();
        try {
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("select * from Sprint_Backlog");
            while (rs.next()) {
                Sprint_Backlog sb = new Sprint_Backlog();
                sb.setCod_sp(rs.getInt("codigo_sp"));
                sb.setNombre_sp(rs.getString("nombre_sp"));
                sb.setCodigo_us(Integer.parseInt(rs.getString("codigo_us")));
                sb.setEstado_sp(rs.getString("estado_sp"));
                
                
                listasp.add(sb);
            }
            rs.close();
            st.close();
        } catch (NumberFormatException | SQLException e) {
        }
        return listasp;
     }
     
//--------------------------------------------------------SPRINT_CABECERA--------------------------------------------------------        

    @RequestMapping(value = "/ConsultarSprint")
    public String ConsultarSprint() {
        return "ConsultaSprint";
    }
     @RequestMapping(value = "/formSprintBacklogC1")
    public String showUserFormSpringBacklogC(Model model) {
        model.addAttribute("sprintbacklogC", new Sprint_BacklogC());
        return "alta_sprintbacklogC";
    }

    @RequestMapping(value = "/insertSprintBacklogC.htm")
    public String insertarSprintBacklogC2(Sprint_BacklogC sb) {
        Conexion();
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("insert into sprint_cabecera values('" +sb.cod_sp
                                                                        + "','"+ sb.nombre_sp
                                                                        + "',to_date('" + sb.fecha_inicio + "','YYYYMMDD')"
                                                                        + " ,to_date('" + sb.fecha_fin + "','YYYYMMDD')" //poner sysdate
                                                                        + " ,'creado');");
            if (z == 1) {
                System.out.println("Se agrego el registro de manera exitosa");
            } else {
                System.out.println("Ocurrio un error en la insercion");
            }
        } catch (SQLException e) {
           return "mensaje_error";
        }
        return "mensaje";
    }

    @RequestMapping(value = "/formSprintBacklogC2")
    public String showUserFormSprintBacklogC2(Model model) {
        model.addAttribute("sprintbacklogC", new Sprint_BacklogC());
        return "baja_sprintbacklogC";
    }

    

    @RequestMapping(value = "/deleteSprintBacklogC.htm")
    public String deleteSprintBacklogC(Sprint_BacklogC sb) {
        Conexion();
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("update sprint_cabecera set estado_sp = 'cancelado' where cod_sprint = '" + sb.cod_sp + "';");
            if (z > 0) {
                System.out.println("Se elimino el registro de manera exitosa");
            } else {
                System.out.println("Ocurrio un error en la eliminacion");
            }

        } catch (SQLException e) {
            return "mensaje_error";
        }
        return "mensaje";
    }

    @RequestMapping(value = "/formSprintBacklogC3")
    public String showUserFormSprintBacklogC3(Model model) {
        model.addAttribute("sprintbacklog", new Sprint_BacklogC());
        return "seleccion_sprintbacklogC";
    }

    /* @RequestMapping(value="/create14")
    public String manejarSBValidado3(Sprint_Backlog sb) {
        if(sb.codigo_proyecto==null || sb.codigo_us ==null){//no se sabe que onda
            return "mensaje_error";//poder cambiar por una vista que imprima mensaje de campo vacio
        }else{
            return selectSprintBacklog(sb).getViewName();
            //return "mensaje";
        } 
    }
     */
    @RequestMapping(value = "/selectSprintBacklogC.htm")
    public ModelAndView selectSprintBacklogC(Sprint_BacklogC sb) {
        Conexion();

        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from sprint_cabecera where cod_sprint ='" + sb.cod_sp +"' and estado_sp != 'cancelado';");
            codSprintA= sb.cod_sp;
            while (rs.next()) {
                sb.nombre_sp = rs.getString(2);
                sb.fecha_inicio = rs.getString(3);
                sb.fecha_fin = rs.getString(4);
                sb.estado_sp = rs.getString(5);
            }

        } catch (NumberFormatException | SQLException e) {
            System.out.println("Error de conexion");
        }
        ModelAndView model = new ModelAndView();
        model.addObject("sprintbacklogC", sb);
        model.setViewName("modificacion_sprintbacklogC");
        return model;
    }

    @RequestMapping(value = "/updateSprintBacklogC.htm")
    public String updateSprintBacklogC(Sprint_BacklogC sb) {
        Conexion();

        try {

            s = connection.createStatement();
            int z = s.executeUpdate("update sprint_cabecera set cod_sprint = '" + sb.cod_sp
                                                            + "',nombre_sp = '" + sb.nombre_sp
                                                            + "',fecha_inicio = to_date('" + sb.fecha_inicio + "','YYYYMMDD')"
                                                            + ",fecha_fin = to_date('" + sb.fecha_fin + "','YYYYMMDD')"
                                                            + " where cod_sprint = '" + codSprintA
                                                            + "' and estado_sp !='" + estado
                                                            + "' ;");
            if (z > 0) {
                System.out.println("Se actualizo el registro de manera exitosa");
            } else {
                System.out.println("Ocurrio un error en la actualizacion");
            }
        } catch (SQLException e) {
           return "mensaje_error";
        }
        return "mensaje";
    }
   
}
