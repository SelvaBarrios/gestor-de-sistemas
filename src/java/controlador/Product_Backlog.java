package controlador;

public class Product_Backlog {
    public Integer codigo_proyecto;
    
    public Integer codigo_us;
    
    public String estado_us;
    
    public String prioridad;
    
    public String tiempo_estimado;
    
    public String tipo_us;
    
    public String nombre_us;

    public Integer getCodigo_proyecto() {
        return codigo_proyecto;
    }

    public void setCodigo_proyecto(Integer codigo_proyecto) {
        this.codigo_proyecto = codigo_proyecto;
    }

    public Integer getCodigo_us() {
        return codigo_us;
    }

    public void setCodigo_us(Integer codigo_us) {
        this.codigo_us = codigo_us;
    }

    public String getEstado_us() {
        return estado_us;
    }

    public void setEstado_us(String estado_us) {
        this.estado_us = estado_us;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public String getTiempo_estimado() {
        return tiempo_estimado;
    }

    public void setTiempo_estimado(String tiempo_estimado) {
        this.tiempo_estimado = tiempo_estimado;
    }

    public String getTipo_us() {
        return tipo_us;
    }

    public void setTipo_us(String tipo_us) {
        this.tipo_us = tipo_us;
    }

    public String getNombre_us() {
        return nombre_us;
    }

    public void setNombre_us(String nombre_us) {
        this.nombre_us = nombre_us;
    }

       
}
