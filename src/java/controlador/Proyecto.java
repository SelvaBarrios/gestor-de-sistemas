
package controlador;

public class Proyecto {
    
    public Integer codigo_proyecto;
    
    public String nombre_proyecto;
    
    public String estado;
    
    public String fecha_inicio;
    
    public String fecha_fin;

    public Integer getCodigo_proyecto() {
        return codigo_proyecto;
    }

    public void setCodigo_proyecto(Integer codigo_proyecto) {
        this.codigo_proyecto = codigo_proyecto;
    }

    public String getNombre_proyecto() {
        return nombre_proyecto;
    }

    public void setNombre_proyecto(String nombre_proyecto) {
        this.nombre_proyecto = nombre_proyecto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(String fecha_fin) {
        this.fecha_fin = fecha_fin;
    }
  
}
