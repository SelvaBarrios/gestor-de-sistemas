
package controlador;

public class Sprint_Backlog {
    
    public Integer cod_sp;
    
    public String nombre_sp;

    public String fecha_limite;
    
    public String estado_sp;
    
    public String fecha_inicio;
    
    public String tiempo_trabajado;
    
    public Integer codigo_proyecto;
    
    public Integer codigo_us;
    
    public String getNombre_sp() {
        return nombre_sp;
    }

    public void setNombre_sp(String nombre_sp) {
        this.nombre_sp = nombre_sp;
    }
    
    public Integer getCod_sp() {
        return cod_sp;
    }

    public void setCod_sp(Integer cod_sp) {
        this.cod_sp = cod_sp;
    }
    
    public String getFecha_limite() {
        return fecha_limite;
    }

    public void setFecha_limite(String fecha_limite) {
        this.fecha_limite = fecha_limite;
    }

    public String getEstado_sp() {
        return estado_sp;
    }

    public void setEstado_sp(String estado_sp) {
        this.estado_sp = estado_sp;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getTiempo_trabajado() {
        return tiempo_trabajado;
    }

    public void setTiempo_trabajado(String tiempo_trabajado) {
        this.tiempo_trabajado = tiempo_trabajado;
    }

    public Integer getCodigo_proyecto() {
        return codigo_proyecto;
    }

    public void setCodigo_proyecto(Integer codigo_proyecto) {
        this.codigo_proyecto = codigo_proyecto;
    }

    public Integer getCodigo_us() {
        return codigo_us;
    }

    public void setCodigo_us(Integer codigo_us) {
        this.codigo_us = codigo_us;
    }
    
}
