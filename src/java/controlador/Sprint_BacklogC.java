/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

/**
 *
 * @author Pc
 */
public class Sprint_BacklogC {

    public Integer cod_sp;
    
    public String nombre_sp;
    
    public String fecha_inicio;
    
    public String fecha_fin;
    
    public String estado_sp;

    public Integer getCod_sp() {
        return cod_sp;
    }

    public void setCod_sp(Integer cod_sp) {
        this.cod_sp = cod_sp;
    }

    public String getNombre_sp() {
        return nombre_sp;
    }

    public void setNombre_sp(String nombre_sp) {
        this.nombre_sp = nombre_sp;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(String fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public String getEstado_sp() {
        return estado_sp;
    }

    public void setEstado_sp(String estado_sp) {
        this.estado_sp = estado_sp;
    }
    
}
