<%--<%@page import= "entrega06.05.controlador.LoginControlador"%>--%>
<%@page import="controlador.Product_Backlog"%>
<%@page import="java.util.LinkedList"%>
<%@page import="controlador.LoginControlador"%>
<!-- Los import -->
<%@ page language="java" %>

<html>
    <body>
        <h1>Dashboard Product Backlog</h1>
        <table border="1">
            <tr>
                <td>Proyecto</td>
                <td>UserStory</td>
                <td>EstadoUS</td>
                <td>Prioridad</td>
                <td>TiempoAprox</td>
                <td>TipoUS</td>
                <td>NombreUS</td>
            </tr>
            <%
                LoginControlador loginController = new LoginControlador();
                LinkedList<Product_Backlog> listaUS = loginController.ConsultaUS();
                
                for (int i = 0; i < listaUS.size(); i++) {
                    out.println("<tr>");
                    out.println("<td>" + listaUS.get(i).getCodigo_proyecto() + "</td>");
                    out.println("<td>" + listaUS.get(i).getCodigo_us() + "</td>");
                    out.println("<td>" + listaUS.get(i).getEstado_us() + "</td>");
                    out.println("<td>" + listaUS.get(i).getPrioridad() + "</td>");
                    out.println("<td>" + listaUS.get(i).getTiempo_estimado() + "</td>");
                    out.println("<td>" + listaUS.get(i).getTipo_us() + "</td>");
                    out.println("<td>" + listaUS.get(i).getNombre_us() + "</td>");
                    out.println("</tr>");
                }
            %>
            
        </table>
    </body>
</html>