<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html xmlns="http://www.w3.org/1999/xhtml">
     <head>
        <title>Registrar Proyecto</title>
    </head>
    <body>
        <h2>Registrar Proyecto:</h2>
        <form:form modelAttribute="proyecto" action="insertProyecto.htm">
            <table>
                <tr>
                    <td>Codigo Proyecto:</td>
                    <td><form:input  path="codigo_proyecto" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td>Nombre Proyecto:</td>
                    <td><form:input  path="nombre_proyecto" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td>Fecha Inicio:</td>
                    <td><form:input  path="fecha_inicio" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td>Fecha Fin:</td>
                    <td><form:input  path="fecha_fin" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td colspan="1">
                        <input type="submit" value="Aceptar" />
                    </td>
                </tr>
            </table>
        </form:form>
    </body>
</html>

