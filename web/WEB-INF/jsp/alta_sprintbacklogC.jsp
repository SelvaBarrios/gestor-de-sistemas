<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Registrar SprintBacklog Cabecera</title>
    </head>
    <body>
        <h2>Registrar SprintBacklog:</h2>
        <mvc:form modelAttribute="sprintbacklogC" action="insertSprintBacklogC.htm">
            <mvc:errors path="*" cssClass="errorblock" element="div"/>
            <table>
                 <tr>
                    <td><mvc:label  path="cod_sp">Codigo Sprint </mvc:label></td>
                    <td><mvc:input  path="cod_sp" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td><mvc:label  path="nombre_sp">Nombre Sprint: </mvc:label></td>
                    <td><mvc:input  path="nombre_sp" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td><mvc:label  path="fecha_inicio">Fecha Inicio: </mvc:label></td>
                    <td><mvc:input  path="fecha_inicio" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td><mvc:label  path="fecha_fin">Fecha Inicio: </mvc:label></td>
                    <td><mvc:input  path="fecha_fin" cssErrorClass="formFieldError" /></td>
                </tr>
                                               
                <tr>
                    <td colspan="1">
                        <input type="submit" value="Aceptar" />
                    </td>
                </tr>
            </table>
        </mvc:form>
        <mvc:form action="ConsultarSprint.htm">
            <table>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="ConsultarUS" />
                    </td>
                </tr>
            </table>
        </mvc:form>
        <mvc:form action="ConsultarPr.htm">
            <table>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="ConsultarProyecto" />
                    </td>
                </tr>
            </table>
        </mvc:form>
    </body>
</html>