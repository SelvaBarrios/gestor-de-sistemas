<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <title>Registrar Usuario</title>
        <style type="text/css">
                body {
                    font-family: verdana, sans-serif;			
                }
                span.campoConError {
                    color: red;
                }		
        </style>	
    </head>  
    
    <body>
	<form:form modelAttribute="usuario" action="insert.htm">
            <h2>Registrar Usuario:</h2>
            <table>
                
                <tr>
                    <td>Cedula:</td>
                    <td><form:input path="cedula" /></td>
                </tr>
                
                <tr>
                    <td>Id Rol:</td>
                    <td><form:input path="id_rol" /></td>
                </tr>
                
                <tr>
                    <td>Nombres: </td>
                    <td><form:input path="nombres" /></td>
                    
                </tr>
                
                <tr>
                    <td>Apellidos: </td>
                    <td><form:input path="apellidos" /></td>
                   
                </tr>
                
                <tr>
                    <td>Nombre de Usuario: </td>
                    <td><form:input path="nombre_usuario" /></td>
                    
                </tr>
                
                <tr>
                    <td>Contraseña: </td>
                    <td><form:input path="contrasenha" /></td>
                    
                </tr>
                
                <tr>
                    <td>Telefono: </td>
                    <td><form:input path="telefono" /></td>
                   
                </tr>
                
                <tr>
                    <td colspan="1"><input type="submit" value="Aceptar" /></td>
                </tr>
            </table>
        </form:form>
    </body>
</html>