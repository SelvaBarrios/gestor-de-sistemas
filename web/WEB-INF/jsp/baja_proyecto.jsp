<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Eliminar Proyecto</title>
    </head>
    <body>
        <h2>Eliminar Proyecto:</h2>
        <mvc:form modelAttribute="proyecto" action="deleteProyecto.htm">
            <table>
                <tr>
                    <td><mvc:label  path="codigo_proyecto">Codigo Proyecto: </mvc:label></td>
                    <td><mvc:input  path="codigo_proyecto" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td colspan="1">
                        <input type="submit" value="Aceptar" />
                    </td>
                </tr>
            </table>
        </mvc:form>
    </body>
</html>
