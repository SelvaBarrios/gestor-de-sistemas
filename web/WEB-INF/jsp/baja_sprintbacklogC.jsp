<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Spring MVC Baja Validacion</title>
    </head>
    <body>
        <h2>Eliminar Sprint Backlog Cabecera:</h2>
        <mvc:form modelAttribute="sprintbacklogC" action="deleteSprintBacklogC.htm">
            <table>
                <tr>
                    <td><mvc:label  path="cod_sp">Codigo Sprint: </mvc:label></td>
                    <td><mvc:input  path="cod_sp" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td colspan="1">
                        <input type="submit" value="Aceptar" />
                    </td>
                </tr>
            </table>
        </mvc:form>
    </body>
</html>
