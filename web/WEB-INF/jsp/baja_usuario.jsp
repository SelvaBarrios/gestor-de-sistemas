<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Spring MVC Baja Validacion</title>
    </head>
    <body>
        <h2>Eliminar Usuario:</h2>
        <mvc:form modelAttribute="usuario" action="delete.htm">
            <table>
                <tr>
                    <td><mvc:label  path="cedula">Cedula: </mvc:label></td>
                    <td><mvc:input  path="cedula" cssErrorClass="formFieldError" /></td>
                    
                </tr>
                <tr>
                    <td colspan="1">
                        <input type="submit" value="Aceptar" />
                    </td>
                </tr>
            </table>
        </mvc:form>
    </body>
</html>
