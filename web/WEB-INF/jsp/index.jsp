<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Login</title>
    </head>

    <body>
    <form action="login.htm">
    <table border="0">
        <tr>
            <td colspan="2" align="center">Login Spring Mvc</td>
        </tr>
        
        <tr>
            <td>Usuario:</td>
            <td><input type="text" name="usuario"></td>
        </tr>
        
        <tr>
            <td>Password:</td>
            <td><input type="password" name="password"></td>
        </tr>
        
        <tr>
            <td colspan="2" align="right">
                <input type="submit" value="Enviar">
            </td>
        </tr>
    </table>
    </form>
    </body>
</html>


<!--
https://www.hazunaweb.com/curso-de-html/tablas/
<table> Para definir tablas 
    border= “x” siendo la x un número. 
    Ese número indicará el grosor del borde. 
    Si no ponemos borde o lo escribimos “0”, 
    la tabla no mostrará borde ninguno
<tr> El encabezado las filas se escriben gracias a las etiquetas 
<td> Las celdas que van dentro de cada fila las tenemos que escribirlas con la etiqueta 
-->
