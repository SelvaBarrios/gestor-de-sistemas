<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <title>Spring MVC Form Validation</title>
        <style type="text/css">
                body {
                    font-family: verdana, sans-serif;			
                }
                span.campoConError {
                    color: red;
                }		
        </style>	
    </head>  
    
    <body>   
        <form:form modelAttribute="productbacklog" action="updateProductBacklog.htm">
            <h2>Modificar Product Backlog:</h2>
            <table>
                
                <tr>
                    <td>Codigo Proyecto</td>
                    <td><form:input  path="codigo_proyecto"/></td>
                </tr>
                
                <tr>
                    <td>Codigo User Story:</td>
                    <td><form:input  path="codigo_us"/></td>
                </tr>
                
               
                
                <tr>
                    <td>Prioridad: </td>
                    <td><form:input  path="prioridad"/></td>
                </tr>
                
                <tr>
                    <td>Tiempo Estimado: </td>
                    <td><form:input  path="tiempo_estimado"/></td>
                </tr>
                
                <tr>
                    <td>Tipo User Story:</td>
                    <td><form:input  path="tipo_us"/></td>
                </tr>
                
                <tr>
                    <td>Nombre User Story:</td>
                    <td><form:input  path="nombre_us"/></td>
                </tr>
                
                <tr>
                    <td colspan="1">
                        <input type="submit" value="Aceptar" />
                    </td>
                </tr>
                
            </table>
        </form:form>
    </body>
</html>