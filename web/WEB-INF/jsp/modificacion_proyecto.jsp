<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Modificar Proyecto</title>
    </head>
    <body>
        <h2>Modificar Proyecto:</h2>
        <mvc:form modelAttribute="proyecto" action="updateProyecto.htm">
            
            <table>
                <tr>
                    <td><mvc:label  path="codigo_proyecto">Codigo Proyecto: </mvc:label></td>
                    <td><mvc:input  path="codigo_proyecto" cssErrorClass="formFieldError" /></td>
                    
                </tr>
                <tr>
                    <td><mvc:label  path="nombre_proyecto">Nombre Proyecto: </mvc:label></td>
                    <td><mvc:input  path="nombre_proyecto" cssErrorClass="formFieldError" /></td>
                    
                </tr>
                
                <tr>
                    <td><mvc:label  path="fecha_inicio">Fecha Inicio: </mvc:label></td>
                    <td><mvc:input  path="fecha_inicio" cssErrorClass="formFieldError" /></td>
                    
                </tr>
                <tr>
                    <td><mvc:label  path="fecha_fin">Fecha Fin: </mvc:label></td>
                    <td><mvc:input  path="fecha_fin" cssErrorClass="formFieldError" /></td>
                    
                </tr>
                
                <tr>
                    <td colspan="1">
                        <input type="submit" value="Aceptar" />
                    </td>
                </tr>
          </table>
       </mvc:form>
    </body>
</html>
