<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Spring MVC Form Validation</title>
    </head>
    <body>
        <h2>Registrar SprintBacklog:</h2>
        <mvc:form modelAttribute="sprintbacklog" action="updateSprintBacklog.htm">
            <mvc:errors path="*" cssClass="errorblock" element="div"/>
            <table>
                <tr>
                    <td><mvc:label  path="codigo_proyecto">Codigo_proyecto: </mvc:label></td>
                    <td><mvc:input  path="codigo_proyecto" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td><mvc:label  path="codigo_us">Codigo_US: </mvc:label></td>
                    <td><mvc:input  path="codigo_us" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td><mvc:label  path="estado_sp">Estado_SP </mvc:label></td>
                    <td><mvc:input  path="estado_sp" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td><mvc:label  path="fecha_limite">Fecha_limite </mvc:label></td>
                    <td><mvc:input  path="fecha_limite" cssErrorClass="formFieldError" /></td>
                </tr>
               
                <tr>
                    <td><mvc:label  path="fecha_inicio">Fecha_inicio </mvc:label></td>
                    <td><mvc:input  path="fecha_inicio" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td><mvc:label  path="Tiempo_trabajado">Tiempo_trabajado </mvc:label></td>
                    <td><mvc:input  path="Tiempo_trabajado" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td colspan="1">
                        <input type="submit" value="Aceptar" />
                    </td>
                </tr>
            </table>
        </mvc:form>
    </body>
</html>



