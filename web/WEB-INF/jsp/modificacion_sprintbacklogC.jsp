<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Spring MVC Form Validation</title>
    </head>
    <body>
        <h2>Registrar SprintBacklog:</h2>
        <mvc:form modelAttribute="sprintbacklogC" action="updateSprintBacklogC.htm">
            <mvc:errors path="*" cssClass="errorblock" element="div"/>
            <table>
                <tr>
                    <td><mvc:label  path="cod_sp">Codigo_proyecto: </mvc:label></td>
                    <td><mvc:input  path="cod_sp" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td><mvc:label  path="nombre_sp">Codigo_US: </mvc:label></td>
                    <td><mvc:input  path="nombre_sp" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td><mvc:label  path="fecha_inicio">Estado_SP </mvc:label></td>
                    <td><mvc:input  path="fecha_inicio" cssErrorClass="formFieldError" /></td>
                </tr>
                <tr>
                    <td><mvc:label  path="fecha_fin">Fecha_limite </mvc:label></td>
                    <td><mvc:input  path="fecha_fin" cssErrorClass="formFieldError" /></td>
                </tr>
                
                <tr>
                    <td colspan="1">
                        <input type="submit" value="Aceptar" />
                    </td>
                </tr>
            </table>
        </mvc:form>
    </body>
</html>



