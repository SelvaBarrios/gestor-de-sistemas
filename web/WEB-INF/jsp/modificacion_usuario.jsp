<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <title>Spring MVC Form Validation</title>
        <style type="text/css">
                body {
                    font-family: verdana, sans-serif;			
                }
                span.campoConError {
                    color: red;
                }		
        </style>	
    </head>  
    
    <body>
	<form:form modelAttribute="usuario" action="update.htm">
            <h2>Modificar Datos del Usuario:</h2>
            <table>
                
                <tr>
                    <td>Cedula:</td>
                    <td><form:input path="cedula" /></td>
                    
                </tr>
                
                <tr>
                    <td>Id Rol:</td>
                    <td><form:input path="id_rol" /></td>
                </tr>
                
                <tr>
                    <td>Nombres: </td>
                    <td><form:input path="nombres" /></td>
                    <td><form:errors  path="nombres" cssClass="campoConError" /></td>
                </tr>
                
                <tr>
                    <td>Apellidos: </td>
                    <td><form:input path="apellidos" /></td>
                    <td><form:errors  path="apellidos" cssClass="campoConError" /></td>
                </tr>
                
                <tr>
                    <td>Nombre de Usuario: </td>
                    <td><form:input path="nombre_usuario" /></td>
                    <td><form:errors  path="nombre_usuario" cssClass="campoConError" /></td>
                </tr>
                
                <tr>
                    <td>Contraseña: </td>
                    <td><form:input path="contrasenha" /></td>
                    <td><form:errors  path="contrasenha" cssClass="campoConError" /></td>
                </tr>
                
                <tr>
                    <td>Telefono: </td>
                    <td><form:input path="telefono" /></td>
                    <td><form:errors  path="telefono" cssClass="campoConError" /></td>
                </tr>
                
                <tr>
                    <td colspan="1"><input type="submit" value="Aceptar" /></td>
                </tr>
            </table>
        </form:form>
    </body>
</html>
